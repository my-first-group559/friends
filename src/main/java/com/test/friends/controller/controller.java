package com.test.friends.controller;

import com.test.friends.entities.Friend;
import com.test.friends.services.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class controller {

    @Autowired
    private FriendService friendService;
    @GetMapping("/")
    public String home()
    {
        return "Welcome";
    }
    @GetMapping("/friend")
    public List<Friend> getAllFriends()
    {
        return friendService.findAllFriends();
    }

    @GetMapping("/friend/{id}")
    public Optional<Friend> getFriends(@PathVariable long id)
    {
        return friendService.findByid(id);
    }

    @PostMapping("/friend")
    public List<Friend> addFriends(@RequestBody Friend fr)
    {
        friendService.savei(fr);
        return friendService.findAllFriends();
    }

    @PutMapping("/friend/{id}")
    public Friend addFriends(@PathVariable long id)
    {
      return friendService.updt(id);
    }

    @DeleteMapping("friend/{id}")
    public List<Friend> removeFriend(@PathVariable long id)
    {
        friendService.del(id);
        return friendService.findAllFriends();
    }
}
