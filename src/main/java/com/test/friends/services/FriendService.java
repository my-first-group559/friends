package com.test.friends.services;
import java.util.List;
import java.util.Optional;

import com.test.friends.entities.Friend;


public interface FriendService {
    public List<Friend> findAllFriends();
    public Optional<Friend> findByid(Long id);
    public void savei(Friend fr);

    public List<Friend> del(Long id);
    public Friend updt (Long id);
}
