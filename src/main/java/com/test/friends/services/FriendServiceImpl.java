package com.test.friends.services;

import com.test.friends.entities.Friend;
import com.test.friends.repository.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FriendServiceImpl implements FriendService{
    @Autowired
    private FriendRepository friendRepository;
    public List<Friend> findAllFriends()
    {
        List<Friend> friends = new ArrayList<Friend>();
        friends = friendRepository.findAll();
        return friends;
    }
    public Optional<Friend> findByid(Long id)
    {
        return friendRepository.findById(id);
    }
    public void savei(Friend fr)
    {
        friendRepository.save(fr);
        return ;
    }

    public List<Friend> del(Long id){
        Optional<Friend> fr1= friendRepository.findById(id);
        if(fr1.isPresent())
        {
            friendRepository.deleteById(id);
        }

        List<Friend> friends=new ArrayList<Friend>();
        friends =friendRepository.findAll();
        return friends;
    }

    public Friend updt(Long id)
    {
        Optional<Friend> fr1=findByid(id);
        if(fr1.isPresent())
        {
            Friend f1=fr1.get();
            String a1=f1.getFirst();
            f1.setFirst("Aashi");
            String a2=f1.getLast();
            f1.setLast("Mahajan");
            friendRepository.save(f1);
            return f1;
        }
        Optional<Friend> op=Optional.empty();
        Friend f2=op.get();
        return f2;

    }
}
